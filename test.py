#!/home/quetalas/anaconda3/bin python3.8
# import warnings


if __name__ == "__main__":
    # warnings.filterwarnings("ignore")
    import math
    from hyperopt import fmin, tpe, hp, Trials

    data_path = sys.argv[1]

    with mlflow.start_run():


        print("Test program")

        mlflow.log_param("path", data_path)
        mlflow.log_metric("metric", 999)

